/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

// first function here:
function personInfo() {
  let fullName = prompt('Enter your full name:');
  let personAge = prompt('Your age :');

  console.log('Hello , ' + fullName);
  console.log('You are ' + personAge + ' years old');
}

personInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/

//second function here:

function favoriteBands() {
  let courses = [
    '1. The Beatles',
    '2. Metallica',
    '3. The Eagles',
    '4. One Direction',
    '5. Eraserheads',
  ];
  console.log(courses[0]);
  console.log(courses[1]);
  console.log(courses[2]);
  console.log(courses[3]);
  console.log(courses[4]);
}

favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/

//third function here:
function favoritMovies() {
  let courses = [
    '1. The Godfather',
    '2. The Godfather, Part II',
    '3. Shawshank Redemptio',
    '4. To Kill a Mockingbird',
    '5. Psycho',
  ];
  let ratings = 'Rotten Tomatoes Rating: ';

  console.log(courses[0]);
  console.log(ratings + '97%');
  console.log(courses[1]);
  console.log(ratings + '96%');
  console.log(courses[2]);
  console.log(ratings + '91%');
  console.log(courses[3]);
  console.log(ratings + '93%');
  console.log(courses[4]);
  console.log(ratings + '96%');
}

favoritMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
function printFriends() {
  alert('Hi! Please add the names of your friends.');
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log('You are friends with:');
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
}

printFriends();
